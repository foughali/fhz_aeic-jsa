#!/bin/bash

# read input file and solve ILP
# Generate a json file as input for the solver
# Generate a sol file with the affinity
python3 ./ilp-solver/main.py -i ./examples/$1.rt -t

# Generate the Uppaal input file in xta
python3 ./rt_to_xta/main.py -i ./examples/$1
