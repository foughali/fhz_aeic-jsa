# produce xta file from rt, wcet and affinity
import os
from pickle import FALSE

WRITE_PEN = 2
READ_PEN = 3

def read_sol(file):
    affinity = {} # {task:core}
    with open(file) as file_object:
        lines = file_object.readlines()
        for line in lines:
            split_line = line.strip('\n').split(" ")
            affinity[split_line[0]] =  split_line[1]
    return affinity

def compute_highest_t(affinty):
    count = {}
    highest_t = 0
    for core in affinty.values():
        if core in count:
            count[core] += 1
            highest_t = max([highest_t, count[core]])
        else:
            count[core] = 1
    return highest_t

def read_input(file):
    with open(file) as file_object:
        lines = file_object.readlines()
        tasks = {}
        tasks_index = {} # (nb:name)
        readers = {}
        data = {} # {name:{in:[], out:[]}}
        dictStates = {} #{name:{wcet:val, bcet:val, task:name}}
        nhrt = []
        for line in lines:
            split_line = line.strip('\n').split(" ")        
            if (split_line[0].startswith('processors')):
                nb_of_processors = int(split_line[1])
            elif (split_line[0].startswith('task')):
                tasks[split_line[1]] = {}
                task_nb = split_line[0][5:]
                tasks[split_line[1]]["id"] = task_nb
                tasks[split_line[1]]["graph"] = {}
                tasks_index[task_nb] = split_line[1]
            elif (split_line[0].endswith('_pr')):
                tasks[split_line[0][:-3]]["priority"] = int(split_line[1])
            elif (split_line[0].endswith('_per')):
                tasks[split_line[0][:-4]]["period"] = float(split_line[1])    
            elif ((split_line[0].endswith('_hrt')) and int(split_line[1]) == 0):
                nhrt.append(str(split_line[0][:-4]))    
            elif (split_line[0].startswith('CS_')):
                val = split_line[0].split("_")
                tasks[tasks_index[val[1]]]["graph"][split_line[1]] = []
                dictStates[split_line[1]] = {}
                dictStates[split_line[1]]["wcet"] = 0
                dictStates[split_line[1]]["bcet"] = 0
                dictStates[split_line[1]]["task"] = tasks_index[val[1]]
                if (val[2] == '1' and  val[3] == '1'):
                    if "starting_node" not in tasks[tasks_index[val[1]]]:
                        tasks[tasks_index[val[1]]]["starting_node"] = []
                    tasks[tasks_index[val[1]]]["starting_node"].append(split_line[1])                
            elif (split_line[0].endswith('_succ_', 0, -1)):   
                tasks[tasks_index[val[1]]]["graph"][split_line[0].split('_succ_')[0]].append(split_line[1])
            # synth grammar
            elif (split_line[0].endswith('_reader')):
                readers[split_line[0][:-7]] = split_line[1]
            # .rt grammar
            elif (split_line[0].endswith('_out')):
                if split_line[1] in data:
                    if 'out' in data[split_line[1]]:
                        data[split_line[1]]['out'].append(split_line[0][:-4])
                    else:
                        data[split_line[1]]['out'] = [split_line[0][:-4]]
                else:
                    data[split_line[1]] = {}
                    data[split_line[1]]['out'] = [split_line[0][:-4]]
            elif (split_line[0].endswith('_in')):
                if split_line[1] in data:
                    if 'in' in data[split_line[1]]:
                        data[split_line[1]]['in'].append(split_line[0][:-3])
                    else:
                        data[split_line[1]]['in'] = [split_line[0][:-3]]
                else:
                    data[split_line[1]] = {}
                    data[split_line[1]]['in'] =  [split_line[0][:-3]]
            elif (split_line[0].endswith('_wcet')):
                dictStates[split_line[0][:-5]]['wcet'] = int(split_line[1])
            elif (split_line[0].endswith('_bcet')):
                if (len(split_line) > 1):
                    dictStates[split_line[0][:-5]]['bcet'] = int(split_line[1])
                else:
                    dictStates[split_line[0][:-5]]['bcet'] = 0
        
        i = 0
        while (i < len(lines)):
            line = lines[i]
            split_line = line.strip('\n').split(" ")
            if (split_line[0].endswith('_penalty')):
                # print("penalty ", split_line[0][:-8], split_line[1])
                data[split_line[0][:-8]]['penalty'] = split_line[1]       
            i += 1
        
        return tasks, nb_of_processors, readers, data, dictStates, nhrt

write_overhead = 1
read_overhead = 2

def update_overhead(data, affinity, dictStates):
    for data_name, data_value in data.items():
        if 'out' in data_value:
            for node_out in data_value['out']:
                nb_overhead = 0
                if 'in' in data_value:
                    for node_in in data_value['in']:
                        if affinity['affinity_'+ dictStates[node_in]['task']] != affinity['affinity_'+ dictStates[node_out]['task']]:                            
                            nb_overhead += 1
                            # print("add penalty write", node_out, data_name)
                            dictStates[node_out]['wcet'] += int(float(data[data_name]['penalty']))
                            break
                ## dictStates[node_out]['wcet'] += nb_overhead*write_overhead                        
        if 'in' in data_value:
            for node_in in data_value['in']:
                nb_overhead = 0
                if 'out' in data_value:
                    if affinity['affinity_'+ dictStates[node_in]['task']] != affinity['affinity_'+ dictStates[node_out]['task']]:
                        nb_overhead += 1
                        # print("add penalty read", node_out, data_name)
                        dictStates[node_out]['wcet'] += int(2*float(data[data_name]['penalty']))
                        break
                # dictStates[node_in]['wcet'] += 2*nb_overhead*write_overhead
                    
    return 0

def list_hyperjobs(tasks):
    for task in tasks.values():        
        task["hyperjobs"] = get_hyperjobs(task)

def get_hyperjobs(task):
    hyperjobs = []
    for start in task["starting_node"]:
        explore(task, start, [], hyperjobs)
    return hyperjobs

def explore(task, node, acc, hyperjobs):
    acc.append(node)
    if (node in task["graph"]):
        for next in task["graph"][node]:        
            explore(task, next, acc.copy(), hyperjobs)
    else:
        hyperjobs.append(acc)

default_wcet = 0.000001
to_int = 1000000

def to_json(file, wcet, tasks, number_of_processors, readers):
    dict_wcet = {}
    try:
        with open(wcet) as file_object:
            lines = file_object.readlines()
            for line in lines:
                split_line = line.strip('\n').split(" ")
                if  len(split_line) > 1:
                    dict_wcet[split_line[0]] = float(split_line[1])
                else:
                    dict_wcet[split_line[0]] = default_wcet
    except IOError as error:
        for name, task in tasks.items():
            for hyperjob in task["hyperjobs"]:
                for codel in hyperjob:
                    dict_wcet[codel] = default_wcet
    
    
    dict_codel_task = {}
    for name, task in tasks.items():
        for hyperjob in task["hyperjobs"]:
            for codel in hyperjob:
                dict_codel_task[codel] = name

    st = "{\"number_of_processors\": " + str(number_of_processors)
    st += ", \"scheduling_policy\" : \"RM\", "
    st += "\"taskset\" : ["
    lst = []
    for name, task in tasks.items():
        lst.append(write_task(task, name, dict_wcet, readers, dict_codel_task))
    st += ", ".join(lst)
    st += "]}"
    return st
        
def write_task(task, name, dict_wcet, readers, dict_codel_task):
    st = "{ \"id\": \"" + name + "\""
    st += ", \"period\": " + str(int(task["period"]*to_int))
    st += ", \"hyperjobs\": ["
    lst = []
    for hyperjob in task["hyperjobs"]:
        lst.append(write_hyperjob(hyperjob, dict_wcet, readers, dict_codel_task))
    st += ",".join(lst)
    st += "]}"
    return st

def write_hyperjob(hyperjob, dict_wcet, readers, dict_codel_task):
    st = "{ \"codels\": ["
    lst = []
    overhead = False    
    for codel in hyperjob:
        lst.append(str(int(float(dict_wcet[codel])*to_int)))
    st += ",".join(lst)    
    st += "]"

    for codel in hyperjob:
        if (codel in readers.keys()):
            overhead = True
            break
    
    if overhead:
        red_over = 100
        st += ", \"overhead\": ["
        lstoverhead = []
        for codel in hyperjob:
            st2 = "{\"value\":"
            if (codel in readers.keys()):
                st2 += str(int(float(dict_wcet[codel])*to_int / red_over) + int(float(dict_wcet[readers[codel]])*to_int / red_over))            
                st2 += ', \"set\": [\"'
                st2 += dict_codel_task[codel]
                st2 += "\"]}"
            else:
                st2 += "0,\"set\":[]}"
            lstoverhead.append(st2)
        st += ",".join(lstoverhead)
        st += "]"
    st += "}"
    return st

def print_codels(tasks):
    for task in tasks.values():
        for codel in task["graph"]:
            print(codel)

def string_xta1(affinity, tasks, nb_of_processors, data):
    highest_t = compute_highest_t(affinity)

    st = "/* highest number of tasks assigned to a given core */\n\
const int ht := {nb_h};\n".format(nb_h = highest_t+1)

    st += "\n/* type for scheduler array cell */\n\
typedef struct {int id; int pr;} tab_core;\n"

    task_per_core = {} # {id_core:[lst task]}
    for k,v in affinity.items():
        if v in task_per_core:
            task_per_core[v].append(k)
        else:
            task_per_core[v] = [k]

    st += "\n/* constants : tasks IDs */\n"
    for core_n in task_per_core:
        id = 0
        for task in task_per_core[core_n]:
            st+="const int {name}_id  := {id};\n".format(name = task[9:], id = id)
            id += 1

    st +="\n/* constants : task priorities */\n"
    for task in tasks:
        st += "const int {name}_pr := {priority};\n".format(name = task, priority = tasks[task]["priority"])

    st += "\n/* variables */\n"
    for n in range(nb_of_processors):
        st += "bool busy_core_{n}[ht] := ".format(n=n)
        l = ["false"]*(highest_t+1)
        st += "{ " + ", ".join(l) + " };\n"
        st += "tab_core_{n}[ht] := ".format(n = n)
        l = ["{-1, -1}"]*(highest_t+1)
        st += "{ " + ", ".join(l) + " };\n"
    st += "\n/* channels */\n"
    for n in range(nb_of_processors):
        st += "chan core_{n}_insert, core_{n}_end, core_{n}_concede;\n".format(n=n)
        st += "broadcast chan core_{n}_procede, core_{n}_exe, core_{n}_ter;\n".format(n=n)
        st += "urgent chan core_{n}_urg[ht];\n".format(n=n)
        st += "chan priority core_{n}_exe < core_{n}_ter, core_{n}_end, core_{n}_concede, core_{n}_procede < core_{n}_insert, core_{n}_urg;\n".format(n=n)
    return st

def string_xta2():
    st = ""
    with open(os.path.join(os.path.dirname(__file__),"xta2.txt")) as file_object:
        lines = file_object.readlines()
        for line in lines:
            st += line
    return st

def string_xta3(affinity, tasks, nb_of_processors, data, dictStates):
    st = ""
    
    task_per_core = {} # {id_core:[lst task]}
    hp = {}
    for k,v in affinity.items():
        if v in task_per_core:
            task_per_core[v].append(k[9:])
        else:
            task_per_core[v] = [k[9:]]
        if v in hp:
            if (tasks[k[9:]]["priority"] > hp[v]):
                hp[v] = tasks[k[9:]]["priority"]
        else:
            hp[v] = tasks[k[9:]]["priority"]
    
    for core_n in task_per_core:
        for task in task_per_core[core_n]:
            if tasks[task]["priority"] == hp[core_n]:
                st += "\n\nprocess task_{name} (const int id, const int pr, chan &insert, chan &end, chan &exe, urgent chan &urg[ht], tab_core &tab[ht])".format(name = task)
                st += " {\n"
            else :
                st += "\n\nprocess task_{name} (const int id, const int pr, chan &insert, chan &end, chan &exe, chan &ter, chan &concede, urgent chan &urg[ht], tab_core &tab[ht])".format(name = task)
                st += "{\n"
            st += "\tclock x,y;\n\tconst int period := {period};\n\tconst int error_tolerance := 2;\n".format(period = int(tasks[task]["period"]))
            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
            c = 1
            while current_nodes:
                state = current_nodes.pop()
                if state.endswith("_ether"):
                    st += "\tbool eth_{c} := false;\n".format(c =c)
                if state.startswith("loop_"):
                    st += "\tbool {state} := false;\n".format(state = state)
                c += 1

                if state.startswith("END_"):
                    c = 1
                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]   
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = []                
            st += "\tstate\n\t\twait {x <= period},\n\t\tact,\n\t\tovershoot {x <= error_tolerance * period},\n"
            
            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
            c = 1
            current_nodes = set(current_nodes)
            while current_nodes:
                state = current_nodes.pop()
                if not state.endswith("_ether") and not state.startswith("loop_"):
                    st += "\t\t{name}".format(name = state)
                    # wcet with overhead
                    if dictStates[state]['wcet'] != 0:
                        st += " {y <= " + str(dictStates[state]['wcet']) + "}"
                    st += ",\n"
                if state.startswith("END_"):
                #    print("===========END")
                    c = 1
                    if (tasks[task]["priority"] < hp[core_n]):
                        #if state not in last segment or (state in last segment and has at least 
                        # one successor that does not start with "END", does not start 
                        # with "loop" and does not end with "ether")>> 
                        if not state.startswith("END_") and not is_state_in_last_segment(state, task, tasks) or (is_state_in_last_segment(state, task, tasks) and has_normal_succ(state, task, tasks)):
                            st +=  "\t\t{state}_concede,\n".format(state = state)

                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]                
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = []
            st +="\t\terror;\n"
            st +="\tcommit\n"

            c = 1
            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
            current_nodes = set(current_nodes)
            while current_nodes:
                state = current_nodes.pop()
                if state.startswith("END_"):
                    st += "\t\t{state},\n".format(state = state)
                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]   
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = []  
            st +="\t\tovershoot;\n"
            st += "\tinit wait;\n"
            st += "\ttrans\n"
            st += "\t\twait -> act "
            st += "{ guard x == period; insert!; assign x:=0, add_to_array (tab, id, pr); },\n"

            act = "act"
            sync = "sync urg[id]?"
            test = True
            first_state = tasks[task]["starting_node"][0]    
            while (test):

                
                # segment       
                st += "\t\t{act} -> {state}".format(act = act, state = first_state)
                st += " { "

                if is_ether_reachable_in_seg(first_state, task, tasks) or (len(list_state_loop(first_state, task, tasks)) > 0):
                    st += "guard !( "
                    lguard = [] 
                    if is_ether_reachable_in_seg(first_state, task, tasks):
                        lguard.append("!eth_{c}".format(c = c))
                    lguard += list_state_loop(first_state, task, tasks)
                    st += " || ".join(lguard)
                    st += ');"'
                st += " {sync}".format(sync = sync)
                st += "; assign y:=0; },\n"            
                if is_ether_reachable_in_seg(first_state, task, tasks):
                    st += "\t\t{act} -> {last_state}".format(act = act, last_state = last_state_in_seg(first_state, task, tasks))
                    st += " { guard "
                    st += "eth_{c};".format(c = c)
                    st +=" {sync}".format(sync = sync)
                    st +=";  },\n"
                for state in list_state_loop(first_state, task, tasks):
                    st += "\t\t{act} -> {state_without_loop}".format(act = act, state_without_loop = state[5:])
                    st += " { "
                    st += "guard {state}; {sync}; assign {state} := false, y:=0;".format(state = state, sync= sync)
                    st+= "  },\n"

                
                for state in list_state_seg(first_state, task, tasks):
                    if (tasks[task]['priority'] < hp[core_n]):
                        if (not state.startswith('END_')) and (not is_state_in_last_segment(state, task, tasks)
                        or (is_state_in_last_segment(state, task, tasks) and (has_normal_succ(state, task, tasks)))):
                            st += "\t\t{state} -> {state}_concede".format(state = state)
                            st += "{ "
                            st +="guard y>= {wcet}; sync concede!; ".format(wcet = dictStates[state]['wcet'])
                            st += "},\n"
                    if state in tasks[task]["graph"]:
                        for succ in tasks[task]["graph"][state]:
                            if (succ.startswith('loop_')):
                                if (tasks[task]['priority'] < hp[core_n]):
                                    if is_state_in_last_segment(succ, task, tasks):
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {wcet}; sync ter!; assign {successor} := true;".format(wcet = dictStates[state]['wcet'], successor = succ)
                                        st += " },\n"
                                    else:
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {wcet}; sync exe!; assign {successor} := true;".format(wcet = dictStates[state]['wcet'], successor = succ)
                                        st += " },\n"
                                else:
                                    st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                    st += " {"
                                    st += "guard y>= {wcet}; sync exe!; assign {successor} := true;".format(wcet = dictStates[state]['wcet'], successor = succ)
                                    st += " },\n"
                            elif (succ.endswith('_ether')):
                                if (tasks[task]['priority'] < hp[core_n]):
                                    st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                    st += " {"
                                    st += "guard y>= {wcet}; sync ter!; assign eth_{c} := true;".format(wcet = dictStates[state]['wcet'], c = c)
                                    st += " },\n"
                                else:
                                    st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                    st += " {"
                                    st += "guard y>= {wcet}; sync exe!; assign eth_{c} := true;".format(wcet = dictStates[state]['wcet'], c = c)
                                    st += " },\n"
                            else:
                                st += " \t\t{state} -> {last}".format(state = state, last = succ)
                                st += " {"
                                st += "guard y>= {wcet}; sync exe!; assign y:=0; ".format(wcet = dictStates[state]['wcet'])
                                st += "},\n"
                                if (tasks[task]['priority'] < hp[core_n]):
                                    st += "\t\t{state}_concede -> {successor}".format(state = state, successor = succ)
                                    st += " {sync urg[id]?; assign y:=0; },\n"
                c += 1                
                
                act = last_state_in_seg(first_state, task, tasks)
                sync = "sync exe!"                
                #first_state = last_state_in_seg(first_state, task, tasks)
                test =  has_successor(act, task, tasks)
                if test:
                    assert len(tasks[task]["graph"][act]) <= 1
                    first_state = tasks[task]["graph"][act][0]

            st += "\t\t{last_state} -> wait ".format(last_state = act)
            st += "{ guard x <= period; sync end!;},\n"
            st += "\t\t{last_state} -> overshoot ".format(last_state = act)
            st += "{ guard x > period && x <= error_tolerance * period; sync end!;},\n"
            st += "\t\t{last_state} -> error ".format(last_state = act)
            st += "{ guard x > error_tolerance * period; sync end!;},\n"
            st += "\t\tovershoot -> act { guard x == error_tolerance * period; sync insert!; assign x:=0, add_to_array (tab, id, pr); };\n}\n"

    st += "\n/* process instantiation */\n\n"

    for core_n in task_per_core:
        st += "scheduler_core_{n} := Scheduler(core_{n}_insert, core_{n}_end, core_{n}_concede, core_{n}_procede,  core_{n}_urg[ht], tab_core_{n}, busy_core_{n});\n".format(n = core_n)
        for task in task_per_core[core_n]:
            if tasks[task]["priority"] == hp[core_n]:
                st += "{task} := task_{task} ({task}_id, {task}_pr, core_{n}_insert, core_{n}_end, core_{n}_exe, core_{n}_urg[ht], tab_core_{n});\n".format(n = core_n, task = task)
            else:
                st += "{task} := task_{task} ({task}_id, {task}_pr, core_{n}_insert, core_{n}_end, core_{n}_exe, core_{n}_ter, core_{n}_concede, core_{n}_urg[ht], tab_core_{n});\n".format(n = core_n, task = task)
    
    st += "\n/* composition */\n"
    st += "\nsystem\n"

    ltask = []
    for core_n in task_per_core:
        st += "scheduler_core_{n},\n".format(n = core_n)
        for task in task_per_core[core_n]:
            ltask.append(task)
    st += ", ".join(ltask)
    st += ";"
    return st



def string_xta_comp(affinity, tasks, nb_of_processors, data, dictStates, num_core, nhrt):
    task_per_core = {} # {id_core:[lst task]}
    hp = {}
    for k,v in affinity.items():
        if v in task_per_core:
            task_per_core[v].append(k[9:])
        else:
            task_per_core[v] = [k[9:]]
        if v in hp:
            if (tasks[k[9:]]["priority"] > hp[v]):
                hp[v] = tasks[k[9:]]["priority"]
        else:
            hp[v] = tasks[k[9:]]["priority"]

    ht = len(task_per_core[str(num_core)])
    st = "/* highest number of tasks assigned to a given core + 1*/\n\
const int ht := {nb_h};\n".format(nb_h = ht + 1)

    st += "\n/* type for scheduler array cell */\n\
typedef struct {int id; int pr;} tab_core;\n"

    st += "\n/* constants : tasks IDs */\n"
    
    id = 0
    for task in task_per_core[str(num_core)]:
        st+="const int {name}_id  := {id};\n".format(name = task, id = id)
        id += 1

    st +="\n/* constants : task priorities */\n"
    for task in task_per_core[str(num_core)]:
        st += "const int {name}_pr := {priority};\n".format(name = task, priority = tasks[task]["priority"])

    st += "\n/* variables */\n"    
    st += "bool busy[ht] := ".format(n=num_core)
    l = ["false"]*(ht+1)
    st += "{ " + ", ".join(l) + " };\n"
    st += "tab_core tab[ht] := ".format(n = num_core)
    l = ["{-1, -1}"]*(ht+1)
    st += "{ " + ", ".join(l) + " };\n"

    st += "\n/* channels */\n"
    st += "chan insert, end, concede;\n".format(n=num_core)
    st += "broadcast chan procede, exe, toend;\n".format(n=num_core)
    st += "urgent chan urg[ht];\n".format(n=num_core)
    st += "chan priority exe < concede;\n".format(n=num_core)
    st += "chan priority procede < insert;\n".format(n=num_core)

    with open(os.path.join(os.path.dirname(__file__),"xta2.txt")) as file_object:
        lines = file_object.readlines()
        for line in lines:
            st += line

    
        for task in task_per_core[str(num_core)]:
            if tasks[task]["priority"] == hp[str(num_core)]:
                st += "\n\nprocess task_{name} (const int id, const int pr)".format(name = task)
                st += " {\n"
            else :
                st += "\n\nprocess task_{name} (const int id, const int pr)".format(name = task)
                st += "{\n"
            st += "\tclock x,y;\n\tconst int period := {period};\n".format(period = int(tasks[task]["period"]))
            if (task in nhrt):
                st += "\tconst int error_tolerance := 2;\n"
            else:
                st += "\tconst int error_tolerance := 1;\n"      
            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
            current_nodes = set(current_nodes)
            c = 1
            alr_visited_nodes = set()
            while current_nodes:
                state = current_nodes.pop()
                if state in alr_visited_nodes:
                    continue
                if state.endswith("_ether"):
                    st += "\tbool eth_{c} := false;\n".format(c =c)
                if state.startswith("loop_"):
                    st += "\tbool {state} := false;\n".format(state = state)

                if state.startswith("END_") and (len(current_nodes) == 0):
                    c += 1
                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]   
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = [] 
                alr_visited_nodes.add(state)               
            st += "\tstate\n\t\tstart,\n\t\twait {x <= period},\n\t\tact,\n\t\tovershoot {x <= error_tolerance * period},\n"
            
            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
#            c = 1
            current_nodes = set(current_nodes)
            alr_visited_nodes = set()
            while current_nodes:
                state = current_nodes.pop()
                if state in alr_visited_nodes:
                    continue
                if not state.endswith("_ether") and not state.startswith("loop_"):
                    st += "\t\t{name}".format(name = state)
                    # wcet with overhead
                    if dictStates[state]['wcet'] != 0:
                        st += " {y <= " + str(dictStates[state]['wcet']) + "}"
                    st += ",\n"
                    if (tasks[task]["priority"] < hp[str(num_core)]):
                        #if state not in last segment or (state in last segment and has at least 
                        # one successor that does not start with "END", does not start 
                        # with "loop" and does not end with "ether")>> 
                        if (not state.startswith('END_')) and (not (is_state_in_last_segment(state, task, tasks))
                        or (is_state_in_last_segment(state, task, tasks) and (has_normal_succ(state, task, tasks)))):
                            st +=  "\t\t{state}_concede,\n".format(state = state)
#                if state.startswith("END_"):
                #    print("===========END")
#                    c = 1

                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]                
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = []
                alr_visited_nodes.add(state) 
            st +="\t\terror;\n"
            st +="\tcommit\n"

            current_nodes = []
            for k in tasks[task]["starting_node"]:                
                current_nodes.append(k)
            next_nodes = []
            current_nodes = set(current_nodes)
            alr_visited_nodes = set()
            while current_nodes:
                state = current_nodes.pop()
                if state in alr_visited_nodes:
                    continue
                if state.startswith('END_'):
                    st += "\t\t{state},\n".format(state = state)
                if state in tasks[task]["graph"]:
                    next_nodes += tasks[task]["graph"][state]   
                if (len(current_nodes) == 0):
                    current_nodes = next_nodes.copy()
                    current_nodes = set(current_nodes)
                    next_nodes = []
                alr_visited_nodes.add(state) 
            st +="\t\nstart;\n"
            st += "\tinit start;\n"
            st += "\ttrans\n"
            st += "\t\twait -> act "
            st += "{ guard x == period; sync insert!; assign x:=0, add_to_array (tab, id, pr); },\n"

            act = "act"
            sync = "sync urg[id]?"
            test = True
            first_state = tasks[task]["starting_node"][0]  
            st +="\t\tstart -> act {sync insert!; assign add_to_array (tab, id, pr);},\n"
            c = 1  
            while (test):

                
                # segment       
                st += "\t\t{act} -> {state}".format(act = act, state = first_state)
                st += " { "
                if is_ether_reachable_in_seg(first_state, task, tasks) or (len(list_state_loop(first_state, task, tasks)) > 0):
                    st += "guard !( "
                    lguard = []                            
                    if is_ether_reachable_in_seg(first_state, task, tasks):
                        lguard.append("eth_{c}".format(c = c))
                    lguard += list_state_loop(first_state, task, tasks)
                    st += " || ".join(lguard)
                    st += " );"
                st += " {sync}".format(sync = sync)
                st += "; assign y:=0; },\n"            
                if is_ether_reachable_in_seg(first_state, task, tasks):
                    st += "\t\t{act} -> {last_state}".format(act = act, last_state = last_state_in_seg(first_state, task, tasks))
                    st += " { guard "
                    st += "eth_{c};".format(c = c)
                    st +=" {sync}".format(sync = sync)
                    st +=";  },\n"
                for state in list_state_loop(first_state, task, tasks):
                    st += "\t\t{act} -> {state_without_loop}".format(act = act, state_without_loop = state[5:])
                    st += " { "
                    st += "guard {state}; {sync}; assign {state} := false, y:=0;".format(state = state, sync= sync)
                    st+= "  },\n"

                av = set()
                for state in list_state_seg(first_state, task, tasks):
                    if state in av:
                        continue
                    if (tasks[task]['priority'] < hp[str(num_core)]):
                        if (not state.startswith('END_')) and (not (is_state_in_last_segment(state, task, tasks))
                        or (is_state_in_last_segment(state, task, tasks) and (has_normal_succ(state, task, tasks)))):
                            st += "\t\t{state} -> {state}_concede".format(state = state)
                            st += "{ "
                            st +="guard y>= {bcet}; sync concede!; ".format(bcet = dictStates[state]['bcet'])
                            st += "},\n"
                    if state in tasks[task]["graph"]:
                        a_v_n = set()
                        for succ in tasks[task]["graph"][state]:
                            if succ in a_v_n:
                                continue
                            if (succ in list_state_loop(state, task, tasks)):
                                if (tasks[task]['priority'] < hp[str(num_core)]):
                                    if is_state_in_last_segment(succ, task, tasks):
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {bcet}; sync toend!; assign {successor} := true;".format(bcet = dictStates[state]['bcet'], successor = succ)
                                        st += " },\n"
                                    else:
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {bcet}; sync exe!; assign {successor} := true;".format(bcet = dictStates[state]['bcet'], successor = succ)
                                        st += " },\n"
                                        if (not state.startswith('END_')):
                                            st += "\t\t{state}_concede -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                            st += " {sync urg[id]?; assign y:=0,"
                                            st += " {successor} := true; ".format(successor = succ)
                                            st += "},\n"
                                else:
                                    st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                    st += " {"
                                    st += "guard y>= {bcet}; sync exe!; assign {successor} := true;".format(bcet = dictStates[state]['bcet'], successor = succ)
                                    st += " },\n"
                            elif (succ.endswith('_ether')):
                                if (tasks[task]['priority'] < hp[str(num_core)]):
                                    if is_state_in_last_segment(succ, task, tasks):
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {bcet}; sync toend!; assign eth_{c} := true;".format(bcet = dictStates[state]['bcet'], c = c)
                                        st += " },\n"
                                    else:
                                        st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                        st += " {"
                                        st += "guard y>= {bcet}; sync exe!; assign eth_{c} := true;".format(bcet = dictStates[state]['bcet'], c = c)
                                        st += " },\n"
                                        if (not state.startswith('END_')):
                                            st += "\t\t{state}_concede -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                            st += " {sync urg[id]?;"
                                            st += " assign eth_{c} := true; ".format(c = c)
                                            st += "},\n"
                                else:
                                    st += " \t\t{state} -> {last}".format(state = state, last = last_state_in_seg(state, task, tasks))
                                    st += " {"
                                    st += "guard y>= {bcet}; sync exe!; assign eth_{c} := true;".format(bcet = dictStates[state]['bcet'], c = c)
                                    st += " },\n"
                            else:
# re-test all conditions! necessary for the generic case (no ether or loop keywords, but end exists, obviously)
                                if (is_state_in_last_segment(state, task, tasks) and succ.startswith('END_')):
                                    st += " \t\t{state} -> {last}".format(state = state, last = succ)
                                    st += " {"
                                    st += "guard y>= {bcet}; sync toend!; assign y:=0; ".format(bcet = dictStates[state]['bcet'])
                                    st += "},\n"
                                else:
                                    st += " \t\t{state} -> {last}".format(state = state, last = succ)
                                    st += " {"
                                    st += "guard y>= {bcet}; sync exe!; assign y:=0; ".format(bcet = dictStates[state]['bcet'])
                                    st += "},\n"
                                    if (tasks[task]['priority'] < hp[str(num_core)]):
                                        st += "\t\t{state}_concede -> {successor}".format(state = state, successor = succ)
                                        st += " {sync urg[id]?; assign y:=0; },\n"
                            a_v_n.add(succ)
                    av.add(state)
                c += 1                
                
                act = last_state_in_seg(first_state, task, tasks)
                sync = "sync exe!"                
                #first_state = last_state_in_seg(first_state, task, tasks)
                test =  has_successor(act, task, tasks)
                if test:
                    assert len(tasks[task]["graph"][act]) <= 1
                    first_state = tasks[task]["graph"][act][0]

            st += "\t\t{last_state} -> wait ".format(last_state = act)
            st += "{ guard x <= period; sync end!;},\n"
            st += "\t\t{last_state} -> overshoot ".format(last_state = act)
            st += "{ guard x > period && x <= error_tolerance * period; sync end!;},\n"
            st += "\t\t{last_state} -> error ".format(last_state = act)
            st += "{ guard x > error_tolerance * period; sync end!;},\n"
            st += "\t\tovershoot -> act { guard x == error_tolerance * period; sync insert!; assign x:=0, add_to_array (tab, id, pr); };\n}\n"

    st += "\n/* process instantiation */\n\n"

    
    st += "scheduler := Scheduler();\n"
    for task in task_per_core[str(num_core)]:
        if tasks[task]["priority"] == hp[str(num_core)]:
            st += "{task} := task_{task} ({task}_id, {task}_pr);\n".format(task = task)
        else:
            st += "{task} := task_{task} ({task}_id, {task}_pr);\n".format(task = task)
    
    st += "\n/* composition */\n"
    st += "\nsystem\n"

    ltask = []    
    for task in task_per_core[str(num_core)]:
        ltask.append(task)
    st += ", ".join(ltask)
    st += ", scheduler; "
    return st
    

def last_state_in_seg(state, task, dic_tasks):
    ret = ""
    if not has_successor(state, task, dic_tasks):
        ret = state
        return ret

    current_nodes = dic_tasks[task]["graph"][state].copy()
    next_nodes = []
    while current_nodes:
        succ = current_nodes.pop(0)
        
        if (succ.startswith('END_')):
            ret = succ
            break
        
        if succ in dic_tasks[task]["graph"]:
            next_nodes += dic_tasks[task]["graph"][succ]
        
        if (len(current_nodes) == 0):
            current_nodes = next_nodes.copy()
            next_nodes = []            

    return ret

# <<for each state excluding states starting with "loop" or "END", or ending with "ether">>
def list_state_seg(state, task, dic_tasks):
    ret = [state]
    if not has_successor(state, task, dic_tasks):
        return ret

    current_nodes = dic_tasks[task]["graph"][state].copy()
    next_nodes = []
    add_end = False
    while current_nodes:
        succ = current_nodes.pop(0)
        if (not succ.startswith('loop_')) and (not succ.startswith('END_')) and (not succ.endswith('_ether')):
            ret.append(succ)

        if (succ.startswith('END_')) and (len(current_nodes) == 0) and (len(next_nodes) == 0):            
            break
        if succ.startswith('END_'):
            add_end = True 
        if succ in dic_tasks[task]["graph"]:
            next_nodes += dic_tasks[task]["graph"][succ]
        
        if (len(current_nodes) == 0):
            if add_end:
                next_nodes += ["END_flag"]
            current_nodes = next_nodes.copy()
            next_nodes = []
            add_end = False 

    return ret

def list_state_loop(state, task, dic_tasks):
    ret = []
    if not has_successor(state, task, dic_tasks):
        ret = []
        return ret

    current_nodes = dic_tasks[task]["graph"][state].copy()
    current_nodes = set(current_nodes)
    alr_visited_nodes = set()
    next_nodes = []
    add_end = False
    while current_nodes:
        succ = current_nodes.pop()
        if succ in alr_visited_nodes:
            continue
        if succ.startswith('loop_'):
            ret.append(succ)
#            break

        if (succ.startswith('END_')) and (len(current_nodes) == 0) and (len(next_nodes) == 0):            
            break
        if succ.startswith('END_'):
            add_end = True 
        if succ in dic_tasks[task]["graph"]:
            next_nodes += dic_tasks[task]["graph"][succ]
        
        if (len(current_nodes) == 0):
            if add_end:
                next_nodes += ["END_flag"]
            current_nodes = next_nodes.copy()
            next_nodes = []
            add_end = False 
        alr_visited_nodes.add(succ)

    return ret

def is_ether_reachable_in_seg(state, task, dic_tasks):
    ret = False
    if not has_successor(state, task, dic_tasks):
        ret = False
        return ret

    current_nodes = dic_tasks[task]["graph"][state].copy()
    next_nodes = []
    add_end = False
    while current_nodes:
        succ = current_nodes.pop(0)
        if succ.endswith('_ether'):
            ret = True
            break

        if (succ.startswith('END_')) and (len(current_nodes) == 0) and (len(next_nodes) == 0):
            ret = False
            break
        if succ.startswith('END_'):
            add_end = True 
        if succ in dic_tasks[task]["graph"]:
            next_nodes += dic_tasks[task]["graph"][succ]
        
        if (len(current_nodes) == 0):
            if add_end:
                next_nodes += ["END_flag"]
            current_nodes = next_nodes.copy()
            next_nodes = []
            add_end = False 

    return ret

def has_successor(state, task, dic_tasks):
    ret = state in dic_tasks[task]["graph"]
    if ret:
        ret = len(dic_tasks[task]["graph"][state]) != 0
    return ret

def has_normal_succ(state, task, dic_tasks):
#has at least one successor that does not start with "END", does not start with "loop" and does not end with "ether")
    if state in dic_tasks[task]["graph"]:
        for succ in dic_tasks[task]["graph"][state]:
            if (not (succ.endswith('_ether') or succ.startswith('loop_') or succ.startswith('END_'))):
                return True
        return False
    else:
        return False

def is_state_in_last_segment(state, task, dic_tasks):
    ret = False
    if not has_successor(state, task, dic_tasks):
        ret = False
        return ret

    current_nodes = dic_tasks[task]["graph"][state].copy()
    next_nodes = []
    while current_nodes:
        succ = current_nodes.pop(0)
        if (succ.startswith('END_') and (len(dic_tasks[task]["graph"][succ]) == 0)):
            ret = True
            break
        elif (succ.startswith('END_') and (len(dic_tasks[task]["graph"][succ]) != 0)):            
            break
        if (len(dic_tasks[task]["graph"][succ]) != 0):
                    next_nodes += dic_tasks[task]["graph"][succ]
        if (len(current_nodes) == 0):
            current_nodes = next_nodes.copy()
            next_nodes = []    

    return ret
    
def string_q(affinity, tasks, nb_of_processors, data, dictStates, num_core):
    task_per_core = {} # {id_core:[lst task]}
    hp = {}
    for k,v in affinity.items():
        if v in task_per_core:
            task_per_core[v].append(k[9:])
        else:
            task_per_core[v] = [k[9:]]
        if v in hp:
            if (tasks[k[9:]]["priority"] > hp[v]):
                hp[v] = tasks[k[9:]]["priority"]
        else:
            hp[v] = tasks[k[9:]]["priority"]

    st = ""
    
    st += "A[] ( "
    for task in task_per_core[str(num_core)]:
        last = ""
        for up in tasks[task]["graph"].items():
            if (up[0].startswith('END_')) and (len(up[1]) == 0):
                last = up[0]
        st += "(not {name}.error) ".format(name = task)
        if task == task_per_core[str(num_core)][-1]:
            st += " )\n "
        else:
            st += " and "
        
    for task in task_per_core[str(num_core)]:
        last = ""
        for up in tasks[task]["graph"].items():
            if (up[0].startswith('END_')) and (len(up[1]) == 0):
                last = up[0]
        st += "A[] not {name}.overshoot\n".format(name = task)
        st += "{name}.start --> {name}.wait\n".format(name = task)
        st += "{name}.wait --> {name}.{last}\n".format(name = task, last = last)
        st += "{name}.{last} --> {name}.wait\n".format(name = task, last = last)
        st += "sup{"
        st += "{name}.{last}".format(name = task, last = last)
        st += "}: "
        st += "{name}.x\n".format(name = task)
    
    return st
