import argparse
import write_xta as wx


def main(file, nodata, c=True): 
    if (c):
        print("start", file, "compostionnal processing")
        affinity = wx.read_sol(file+".sol")
        if not affinity:
            print("The affinity file is not valid, please check it")
            return -1
        tasks, nb_of_processors, readers, data, dictStates, nhrt = wx.read_input(file+".rt")   
        if (not nodata):
            wx.update_overhead(data, affinity, dictStates)
        for n in range(nb_of_processors):
            st = ""
            st += wx.string_xta_comp(affinity, tasks, nb_of_processors, data, dictStates, n, nhrt)
            f = open(file+'_'+ str(n) +'.xta', 'w')
            f.write(st)
            f.close()
            st = wx.string_q(affinity, tasks, nb_of_processors, data, dictStates, n)
            f = open(file+'_'+ str(n) +'.q', 'w')
            f.write(st)
            f.close()
    else:
        print("Need to use option -c")
    return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Project name (without extension)", nargs='?', const='work', default='work')
    parser.add_argument("-nd", "--nodata", help="Do not consider data sharing", action="store_true", default=False)
    args = parser.parse_args()
    main(args.input, args.nodata)
