Source code contains:
- `main.py`
- `write_xta.py`
- `xta2.txt`


You need as input files :
- .rt file with the descitpion of the system (e.g. robotnik.rt)
- .sol file with the task affinity (e.g. robotnik.sol)

To generate .xta file, you have to call:
```
python3 main.py -i [name of the project without extension] [-c|-g]
```
where options -c and -g are mandatory (-c for compositionnal xta and -g for global)

For example:
```
python3 main.py -i robotnik -c
```
