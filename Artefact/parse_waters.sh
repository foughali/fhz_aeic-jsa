#!/bin/bash

cd AUTOSAR
# parse model
python3 parse.py -o ../examples/waters.rt

#parse affinity
python3 parse_sol.py -o ../examples/waters.sol

#return to parent repo
cd ..
