# Model of a task system
from enum import Enum

MAX_NB_ELT = 5

DataMode = Enum('DataMode', ['r','w','rw'])  
SchedulingPolicy = Enum('SchedulingPolicy', ['RM','FP', 'NA'])    

class CriticalSection:
    def __init__(self, id, wcet,  dataset,  bcet):
        self.id = id
        self.dataset = dataset
        self.wcet = wcet
        self.bcet = bcet

    def __str__(self):
        return self.id    

class Data:
    def __init__(self, id, readers, writers, reading_penalty, writing_penalty):
        self.id = id
        self.readers = readers
        self.writers = writers
        self.reading_penalty = reading_penalty
        # self.writing_penalty = writing_penalty
        self.writing_penalty = self.reading_penalty
    def __str__(self):
        return self.id    

class Job:
    def __init__(self, id, critical_sections, period):
        self.id = id
        self.critical_sections = critical_sections
        self.period = period
        self.wcet = 0
        for cs in self.critical_sections:
            self.wcet += cs.wcet

        self.final = critical_sections[-1]
        self.utilization = self.wcet / self.period
    
    def __str__(self):
        st = "\t\tJob:\t"
        data_penalty = 0
        for cs in self.critical_sections[:min(MAX_NB_ELT,len(self.critical_sections))]:
            st += str(cs) + ", "
            for d in cs.dataset:
                data_penalty += d.reading_penalty
        if (len(self.critical_sections) > MAX_NB_ELT):
            st += "...\n"
        else:
            st += "\n"
        print(self.id, self.wcet)
        st += "\t\t\t{ wcet:  "+ str(self.wcet) + \
            ", final:  "+ str(self.final) + \
            ", utilization:  "+ str(self.utilization)
        if (self.wcet != 0):
            st += ", data ex. time: " + str(data_penalty) + " (" + str(round(data_penalty/self.wcet,4)) + "%)"
        st +=  "}"
        return st

class FSM:
    def __init__(self, states, act, end, transitions):
        self.states = states
        self.act = act
        self.end = end
        self.transitions = transitions
    
    def findJobs(self, id, period):
        nodeslist = []
        for node in self.transitions[self.act]:
            self.explore(node, [], nodeslist)
        jobslist = []
        cmpt = 0
        for l in nodeslist:
            jobslist.append(Job(id+str(cmpt), l, period))
            cmpt += 1
        return jobslist

    def explore(self, node, acc, nodeslist):    
        acc.append(node)
        for next in self.transitions[node]:
            if (next != self.end):
                self.explore(next, acc.copy(), nodeslist)
            else:
                nodeslist.append(acc)

class Task:
    def __init__(self, id: str, period: float, fsm: FSM, priority: int = 0, isHRT: bool = False):
        self.id = id
        self.period = period
        self.fsm = fsm
        self.isHRT = isHRT

        self.jobset = fsm.findJobs(self.id, self.period)

        self.wcet = max(self.jobset, key=lambda item: item.wcet).wcet
        self.utilization = self.wcet / self.period

        self.minimum_final_critical_section = min(self.jobset, key=lambda item: item.final.wcet).final.wcet
        self.maximum_critical_section = max(fsm.states, key=lambda item: item.wcet)

        self.priority = priority    
        self.higher_priority = []
        self.same_priority = []
        self.lower_priority= []         
        self.overhead_set = []
        

    def __str__(self):
        st = "Task " + self.id + ":\n"
        st += "\tperiod: " + str(self.period) + "\n"
        st += "\tJobs:\n"
        for h in self.jobset[:min(MAX_NB_ELT, len(self.jobset))]:
            st += str(h) + "\n"        
        st += "\twcet: " + str(self.wcet) + "\n"
        st += "\tutilization: " + str(self.utilization) + "\n"        
        st += "\tminimum_final: " + str(self.minimum_final_critical_section) + "\n"
        st += "\tmaximum_codel: " + str(self.maximum_critical_section) + "\n"        
        st += "\tpriority: " + str(self.priority)
        if (self.isHRT) :
            st += " (HRT)" + "\n"
        else:
            st += "\n"
        l = []
        for t in self.higher_priority[:min(MAX_NB_ELT, len(self.higher_priority))]:
            l.append(t.id)
        st += "\thigher_priority: " + ", ".join(l)
        if (len(self.higher_priority) > MAX_NB_ELT):
            st += "...\n"
        else:
            st += "\n"
        l = []
        for t in self.same_priority:
            l.append(t.id)
        st += "\tsame_priority: " + ", ".join(l) + "\n"
        l = []
        for t in self.lower_priority:
            l.append(t.id)
        st += "\tlower_priority: " + ", ".join(l) + "\n"
        return st  

class Problem:
    number_of_processors = 0
    scheduling_policy = ""

    def __init__(self, number_of_processors, scheduling_policy, taskset, dataset):
        self.number_of_processors = number_of_processors
        self.scheduling_policy = scheduling_policy
        self.taskset = taskset
        self.dataset = dataset
        self.dict_name_idx = {}
        self.update_problem()
        self.create_dictCriticalSection()
    
    def create_dictCriticalSection(self):
        self.dictOfCriticalSection = {}
        for t in self.taskset:
            for cs in t.fsm.states:
                self.dictOfCriticalSection[cs] = t

    def update_problem(self):
        if (self.scheduling_policy == "RM"):
            self.taskset.sort(key=lambda x: x.period, reverse=False)
            prio = len(self.taskset)+1
            self.taskset[0].priority = prio
            for idx in range(1, len(self.taskset)):
                if (self.taskset[idx-1].period != self.taskset[idx].period):
                    prio -= 1
                self.taskset[idx].priority = prio
        
        self.taskset.sort(key=lambda x: x.priority, reverse=False)

        idx = 0
        for task in self.taskset:
            task.idx = idx
            self.dict_name_idx[task.id] = task.idx
            idx += 1
             
            task.higher_priority = []
            task.same_priority = []
            task.lower_priority= [] 
            for task2 in self.taskset:
                if (task != task2):
                    if (task.priority > task2.priority):
                        task.lower_priority.append(task2)
                    elif (task.priority == task2.priority):
                        task.same_priority.append(task2)
                    else:
                        task.higher_priority.append(task2)
        
    def __str__(self) -> str:
        st = "Number of processors: " + str(self.number_of_processors) + "\n"
        st += "Scheduling policy: " + str(self.scheduling_policy) + "\n"
        st += "".join([str(x) for x in self.taskset])
        return st

    def remove_last(self):
        last = None
        for task in self.taskset:
            if (not task.isHRT):
                self.taskset.remove(task)
                last = task
                break
        if (last != None):
            # clean dataset and criticalsection
            for data in self.dataset:
                for cs in last.fsm.states:
                    if cs in data.readers:
                        data.readers.remove(cs)
                    if cs in data.writers:
                        data.writers.remove(cs)
            self.update_problem()
            self.create_dictCriticalSection()
        return last
    
    def max_priotity(self):
        maxprio = max(self.taskset, key=lambda item: item.priority).priority
        return maxprio

def cf(s: CriticalSection, l :Data, m: Problem) -> set:
    res = set()
    if l in s.dataset:
        for cs in l.readers + l.writers:
            if cs != s:
                res.add(m.dictOfCriticalSection[cs])
    return res

def penalty(s: CriticalSection, l :Data):    
    p = 0.0
    if (s in l.readers):
        p += 2.0 * l.reading_penalty
    if (s in l.writers):
        p += l.writing_penalty
    return p
