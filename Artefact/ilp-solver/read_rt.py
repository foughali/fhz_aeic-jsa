import model
# Rewriting an .rt file to a .json file


def read_input(file):
    m = model.Problem(0, 'FP', [], [])

    with open(file) as file_object:
        lines = file_object.readlines()
        for line in lines:
            split_line = line.strip('\n').split(" ")
            if (split_line[0].startswith('processors')):
                m.number_of_processors = int(split_line[1])
        i = 0
        globalcs = {}
        while (i < len(lines)):
            line = lines[i]
            split_line = line.strip('\n').split(" ") 
            begin_task = i
            if (split_line[0].startswith('task')):
                # new task
                begin_task = i
                taskid = split_line[1]
                taskperiod = 0
                taskpriority = 0
                isHRT = True
                # find CS in task
                dictcs = {}
                act = model.CriticalSection('act', 0, list(),0)
                end = model.CriticalSection('end', 0, list(),0)
                # period et priority
                i = begin_task +1
                while (i < len(lines)):
                    line = lines[i]
                    split_line = line.strip('\n').split(" ") 
                    if (split_line[0].startswith('task')):
                        break
                    else:
                        if (split_line[0].endswith('_pr')):
                            taskpriority = int(split_line[1])
                        elif (split_line[0].endswith('_per')):
                            taskperiod = float(split_line[1]) 
                        elif (split_line[0].endswith('_hrt')):
                            if (int(split_line[1]) == 0):
                                isHRT = False
                    i += 1
                i = begin_task +1
                while (i < len(lines)):
                    line = lines[i]
                    split_line = line.strip('\n').split(" ") 
                    if (split_line[0].startswith('task')):
                        break
                    else:
                        if (split_line[0].startswith('CS_')):
                            dictcs[split_line[1]] = model.CriticalSection(split_line[1], 0, list(),0)
                            globalcs[split_line[1]] = dictcs[split_line[1]]
                        elif (split_line[0].endswith('_wcet')):
                            dictcs[split_line[0][:-5]].wcet = int(split_line[1])                
                    i += 1
                # find transitions for a task
                transitions = {}
                transitions[act] = []
                i = begin_task +1
                while (i < len(lines)):
                    line = lines[i]
                    split_line = line.strip('\n').split(" ") 
                    if (split_line[0].startswith('task')):
                        break
                    else:
                        if (split_line[0].endswith('_succ_', 0, -1)):
                            if dictcs[split_line[0].split('_succ_')[0]] not in transitions.keys():
                                transitions[dictcs[split_line[0].split('_succ_')[0]]] = []
                            transitions[dictcs[split_line[0].split('_succ_')[0]]].append(dictcs[split_line[1]])
                    i += 1
                lt = []
                for t in transitions.values():
                    lt += t
                for cs in dictcs.values():
                    if cs not in lt:
                        transitions[act].append(cs)
                    if (cs not in transitions.keys()):
                        transitions[cs] = [end]
                # end task
                fsm = model.FSM(dictcs.values(), act, end, transitions)
                task = model.Task(taskid, taskperiod, fsm, taskpriority, isHRT)
                m.taskset.append(task)
            
            i = begin_task +1
        # Data
        i = 0
        # print(globalcs) 
        dictdata = {}
        while (i < len(lines)):
            line = lines[i]
            split_line = line.strip('\n').split(" ")            
            if (split_line[0].endswith('_out')):
                if split_line[1] not in dictdata.keys():                    
                    dictdata[split_line[1]] = model.Data(split_line[1], [globalcs[split_line[0][:-4]]], [],0,0)
                else:
                    dictdata[split_line[1]].readers.append(globalcs[split_line[0][:-4]])
            elif (split_line[0].endswith('_in')):
                if split_line[1] not in dictdata.keys():
                    dictdata[split_line[1]] = model.Data(split_line[1], [], [globalcs[split_line[0][:-3]]],0,0)
                else:
                    dictdata[split_line[1]].writers.append(globalcs[split_line[0][:-3]])
            i += 1
        i = 0
        while (i < len(lines)):
            line = lines[i]
            split_line = line.strip('\n').split(" ")
            if (split_line[0].endswith('_penalty')):
                dictdata[split_line[0][:-8]].reading_penalty = float(split_line[1])
                dictdata[split_line[0][:-8]].writing_penalty = float(split_line[1])
            i += 1
        
        m.dataset = list(dictdata.values())
        for data in m.dataset:
            for cs in data.readers:
                if cs not in cs.dataset:
                    cs.dataset.append(data)        
            for cs2 in data.writers:  
                if data not in cs2.dataset:
                    cs2.dataset.append(data)
        
        m.update_problem()
        m.create_dictCriticalSection()
    return m
