from telnetlib import NOP
from xmlrpc.client import INVALID_XMLRPC
import gurobipy as gp
from gurobipy import GRB
import model

BIG_M = 1000000

TIME_LIM = 300.0

def get_period(elem):
    return elem.get('period')

def decomp(n):
    L = dict()
    k = 2
    while n != 1:
        exp = 0
        while n % k == 0:
            n = n // k
            exp += 1
        if exp != 0:
            L[k] = exp
        k = k + 1
        
    return L

def _ppcm(a,b):
    Da = decomp(a)
    Db = decomp(b)
    p = 1
    for facteur , exposant in Da.items():
        if facteur in Db:
            exp = max(exposant , Db[facteur])
        else:
            exp = exposant
        
        p *= facteur**exp
        
    for facteur , exposant in Db.items():
        if facteur not in Da:
            p *= facteur**exposant
            
    return p

def ppcm(L):
    while len( L ) > 1:
        a = L[-1]
        L.pop()
        b = L[-1]
        L.pop()
        L.append( _ppcm(a,b) )
        
    return L[0]

def create_model(pb, verbose=False, print_model=False, lequality = None):
    with gp.Env(empty=True) as env:
        env.setParam('OutputFlag', verbose)
        env.start()
        with gp.Model(env=env) as m:            
            

            d_TaskId = {}
            d_CriticalSectionId = {}
            idt = 0
            idcs = 0
            for task in pb.taskset:
                d_TaskId[task] = idt
                idt += 1
                for cs in task.fsm.states:
                    d_CriticalSectionId[cs] = idcs
                    idcs += 1
            d_DataId = {}
            idd = 0
            for data in pb.dataset:
                d_DataId[data] = idd
                idd += 1
            d_JobId = {}
            idj = 0
            for t in pb.taskset:
                for job in t.jobset:
                    d_JobId[job] = idj
                    idj += 1
            
            if (verbose):
                print(">>>> Create varibales alpha <<<<")
            # alpha[i,j] indicate whether High task i is allocated to core j
            alpha = m.addVars(len(pb.taskset), pb.number_of_processors, \
                vtype=GRB.BINARY, name="alpha")
            
            if (verbose):
                print(">>>> Add allocation constraints <<<<")
            # Each task is allocated to only one core
            for task in pb.taskset:
                t = d_TaskId[task]
                m.addConstr( gp.quicksum(
                                alpha[t, c]
                                for c in range(pb.number_of_processors)
                            ) == 1
                )

            if (verbose):
                print(">>>> Add utilization constraints <<<<")
            # Utilization is less than 1 on each processor
            for c in range(pb.number_of_processors):
                m.addConstr( gp.quicksum(
                                alpha[d_TaskId[task], c] * (
                                    task.utilization                            
                                    )
                                for task in pb.taskset
                            ) <= 1
                )

            if (verbose):
                print(">>>> Add symmetry constraints <<<<")
            # Symmetry breaker constraint
            for c in range(1, pb.number_of_processors):
                for t in range(1, len(pb.taskset)):
                    m.addConstr(
                        alpha[t, c]
                        <= gp.quicksum(alpha[t-1, k] for k in range(c))
                    )
    
            if (verbose):    
                print(">>>> Create varibales beta <<<<")
            # create variable beta [s,l,p]
            tlist = []
            for c in range(pb.number_of_processors):
                for cs in d_CriticalSectionId.keys():
                    s = d_CriticalSectionId[cs]
                    for data in cs.dataset:
                        l = d_DataId[data]
                        tlist.append((s, l, c))
            beta = m.addVars(gp.tuplelist(tlist), vtype=GRB.BINARY, name="beta")
        
            if (verbose):
                print(">>>> Add constraints on beta <<<<")
            # Constraints on beta
            for c in range(pb.number_of_processors):
                for cs in d_CriticalSectionId.keys():
                    s = d_CriticalSectionId[cs]
                    task =  pb.dictOfCriticalSection[cs]
                    t = d_TaskId[task]
                    for data in cs.dataset:
                        l = d_DataId[data]    
                        lbeta = []
                        lbeta.append(alpha[t, c])
                        for taskp in model.cf(cs, data, pb):
                            lbeta.append(alpha[d_TaskId[taskp], c])
                        m.addConstr(beta[s, l, c] == gp.and_(lbeta))
                    
            if (verbose):
                print(">>>> Create varibales gamma <<<<")
            # create variable gamma [s,c]
            gamma = m.addVars(len(d_CriticalSectionId), pb.number_of_processors, \
                vtype=GRB.CONTINUOUS, name="gamma")

            if (verbose):
                print(">>>> Add constraints on gamma <<<<")
            # Constraints on gamma
            for cs in d_CriticalSectionId.keys():
                s = d_CriticalSectionId[cs]
                task =  pb.dictOfCriticalSection[cs]
                t = d_TaskId[task]
                for l in [d_DataId[l] for l in cs.dataset]:
                    data = [k for k, v in d_DataId.items() if v == l][0]
                    # print(task.id, t, cs, cs.wcet, data, model.penalty(cs, data), task.period)
                for c in range(pb.number_of_processors):
                    m.addConstr(
                        (alpha[t, c] == 0) >>
                            (
                                gamma[s, c] == 0
                            )
                    )
                    m.addConstr(
                        (alpha[t, c] == 1) >>
                            (
                                gamma[s, c] == cs.wcet
                                    + gp.quicksum(
                                            (1 - beta[s, l, c]) * model.penalty(cs, [k for k, v in d_DataId.items() if v == l][0])
                                            for l in [d_DataId[l] for l in cs.dataset]
                                        )
                            )
                    )

            if (verbose):
                print(">>>> Create varibales epsilon <<<<")
            # Create variable epsilon [j, p]
            epsilon = m.addVars(len(d_JobId), pb.number_of_processors, \
                vtype=GRB.CONTINUOUS, name="epsilon")

            if (verbose):
                print(">>>> Add constraints on epsilon <<<<")
            # General constraint for epsilon
            for job in d_JobId.keys():
                j = d_JobId[job]
                for c in range(pb.number_of_processors):
                    m.addConstr(
                        epsilon[j, c] == gp.quicksum(
                                gamma[s, c]
                                for s in [d_CriticalSectionId[cs] for cs in job.critical_sections]
                            )
                    )
            
            if (verbose):
                print(">>>> Create varibales zeta <<<<")
            # Create variable zeta[j, p]
            zeta = m.addVars(len(d_JobId), pb.number_of_processors, \
                vtype=GRB.CONTINUOUS, name="zeta")

            if (verbose):
                print(">>>> Add constraints on zeta <<<<")
            # General constraint for zeta
            for job in d_JobId.keys():
                j = d_JobId[job]
                for c in range(pb.number_of_processors):
                    m.addConstr(
                        zeta[j, c] == gamma[d_CriticalSectionId[job.final], c]
                    )

            if (verbose):
                print(">>>> Create varibales nu <<<<")
            # Create variable nu [t,c]
            nu = m.addVars(len(pb.taskset), pb.number_of_processors, \
                vtype=GRB.CONTINUOUS, name="nu")
            
            if (verbose):
                print(">>>> Add constraints on nu <<<<")
            # General constraint for nu
            for c in range(pb.number_of_processors):
                for task in pb.taskset:
                    t = d_TaskId[task]                
                    lnu = []
                    if task.lower_priority:
                        for task_l in task.lower_priority:
                            for cs in task_l.fsm.states:
                                s = d_CriticalSectionId[cs]
                                lnu.append(gamma[s, c])
                        m.addConstr(nu[t, c] == gp.max_(lnu))
                    else :
                        m.addConstr(nu[t, c] == 0.0)

            if (verbose):
                print(">>>> Create varibales delta <<<<")
            # Create variable delta [t,c]
            delta = m.addVars(len(pb.taskset), pb.number_of_processors, \
                vtype=GRB.CONTINUOUS, name="delta")

            if (verbose):
                print(">>>> Add constraints on delta <<<<")
            # General constraint for delta
            for task in pb.taskset:
                t = d_TaskId[task]
                for c in range(pb.number_of_processors):
                    ldelta = []
                    for job in task.jobset:
                        j = d_JobId[job]
                        ldelta.append(epsilon[j, c])
                    m.addConstr(delta[t, c] == gp.max_(ldelta))


            if (verbose):
                print(">>>> Add schedulability constraints <<<<")
            # Schedulabity constraints
            for c in range(pb.number_of_processors):
                for task in pb.taskset:
                    t = d_TaskId[task]
                    for job in task.jobset:
                        j = d_JobId[job]
                        if task.lower_priority:
                            for lowtask in task.lower_priority:
                                t_low = d_TaskId[lowtask]
                                m.addConstr(
                                    (alpha[t,c] == 1) >> 
                                        (                                         
                                            nu[t_low, c]
                                            + epsilon[j, c]
                                            + gp.quicksum(
                                                delta[d_TaskId[task_h], c] * (1 + task.period / task_h.period)
                                                for task_h in task.higher_priority
                                            )                                            
                                            - gp.quicksum(
                                                task_h.utilization * zeta[j, c]
                                                for task_h in task.higher_priority
                                            )
                                            - gp.quicksum(
                                                task_h.utilization *  delta[d_TaskId[task_h], c]
                                                for task_h in task.higher_priority
                                            )
                                            + gp.quicksum(
                                                delta[d_TaskId[task_s], c]
                                                for task_s in task.same_priority
                                            )                      
                                            <= task.period
                                        )                              
                                )       
                        else:
                            m.addConstr(                                
                                (alpha[t,c] == 1) >> 
                                    (
                                        epsilon[j,c]
                                        + gp.quicksum(
                                                delta[d_TaskId[task_h], c] * (1 + task.period / task_h.period)
                                                for task_h in task.higher_priority
                                            )                                            
                                            - gp.quicksum(
                                                task_h.utilization * zeta[j, c]
                                                for task_h in task.higher_priority
                                            )
                                            - gp.quicksum(
                                                task_h.utilization *  delta[d_TaskId[task_h], c]
                                                for task_h in task.higher_priority
                                            )
                                            + gp.quicksum(
                                                delta[d_TaskId[task_s], c]
                                                for task_s in task.same_priority
                                            )                      
                                            <= task.period
                                    )
                            )
              
            if (verbose):
                print(">>>> Add equality constraints <<<<")
            # General constraint for delta
            if (lequality):
                for eq in lequality.split(","):
                    seq = eq.split("=")
                    for task in pb.taskset:
                        if (task.id == seq[0]):
                            t0 = d_TaskId[task]
                        if (task.id == seq[1]):
                            t1 = d_TaskId[task]
                    print(t0, t1)
                    for c in range(pb.number_of_processors):
                        m.addConstr(alpha[t0, c] == alpha[t1, c])
            
            if (verbose):
                print(">>>> Add objective constraints <<<<")
            # objective    
            obj = m.addVar(vtype=GRB.INTEGER, name="obj")
            z = m.addVars(pb.number_of_processors, vtype=GRB.INTEGER, name="z")
            periods = []
            for t in pb.taskset:
                periods.append(t.period)
            hyperperiod = ppcm(periods)

            for p in range(pb.number_of_processors):
                m.addConstr(
                    z[p] == gp.quicksum(
                        alpha[d_TaskId[task], p] * task.wcet * hyperperiod / task.period                        
                        for task in pb.taskset
                    )
                )

            m.addConstr(obj == gp.max_(z))
            m.setObjective(obj, GRB.MINIMIZE)
            #m.params.NonConvex = 2

            m.Params.timeLimit = TIME_LIM
            if (print_model):
                m.write('model_ilp.lp')
            

            # Optimize model
            m.optimize()

            succed = (m.status == gp.GRB.OPTIMAL)

            if ((m.status == gp.GRB.OPTIMAL) and verbose) :
                for v in m.getVars():
                    print('%s %g' % (v.VarName, v.X))
                print('Obj: %g' % m.ObjVal)
                print('Max utilization factor: %g' % (m.ObjVal/hyperperiod))

            # print solution file
            st = ""
            if (m.status == gp.GRB.OPTIMAL):
                print("Model is feasible")
                u_per_proc = []
                for proc in range(pb.number_of_processors):
                    u = 0
                    for idx in range(len(pb.taskset)):
                        if (alpha[idx,proc].X >= 0.9):                
                            st += "affinity_" + pb.taskset[idx].id + " " + str(proc) + "\n"
                            u += pb.taskset[idx].utilization
                    u_per_proc.append(u)
                print(st)
                print('Max utilization factor: %g' % (m.ObjVal/hyperperiod))

            elif  (m.status == gp.GRB.TIME_LIMIT):
                print("Time limit")
                if m.SolCount > 0:
                    u_per_proc = []
                    for proc in range(pb.number_of_processors):
                        u = 0
                        for idx in range(len(pb.taskset)):
                            if (alpha[idx,proc].X == 1):                
                                st += "affinity_" + pb.taskset[idx].id + " " + str(proc) + "\n"
                                u += pb.taskset[idx].utilization
                        u_per_proc.append(u)
                    print(st)          
                    print('Max utilization factor: %g' % (m.ObjVal/hyperperiod))

            else:
                print("Model is infeasible")
                st += "Model is infeasible"
    
    return st, succed
    

def write_model(m):
    print (m.write('model.lp'))

