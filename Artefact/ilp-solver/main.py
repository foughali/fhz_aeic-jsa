import argparse
import solver
import read_rt

def main(file, verbose, model, show, try_and_error, beta, lequality) -> int:
    print("-- Import model")
    pb = read_rt.read_input(file)

    if (show):
        print(pb)
    
    succed = False

    lremove = []
    while(True):
        if (beta):
            print("-- beta version is not available")            
        else:
            print("-- Solve ILP problem with shared data")
            m, succed = solver.create_model(pb, verbose, model, lequality)
            f = open(file.replace('.rt', '.sol'), 'w', encoding="utf-8")
            f.write(m)
            f.close()
            
        if try_and_error and not succed:
            last = pb.remove_last()
            if (last == None or last.priority == pb.max_priotity()):
                print("[Infeasible problem] no solution with all HRT")
                break
            else:
                print("[Infeasible problem] remove ", last.id)
                lremove.append(last.id)
        else:
            break
    
    if lremove:
        f = open(file.replace('.rt', '.sol'), 'a', encoding="utf-8")
        st = "/* Remove this line after fixing the file */\n"
        for t in lremove:
            st += "affinity_" + t + " /* This task has not been allocated, add manually its affinity */\n"
        f.write(st)
        f.close()

    return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, help="input file to parse")
    parser.add_argument("-v", "--verbose", help="print solver step", action="store_true")
    parser.add_argument("-m", "--model", help="write ILP model to a file", action="store_true")
    parser.add_argument("-s", "--show", help="Show task model", action="store_true")
    parser.add_argument("-b", "--beta", help="Experimental constraint version", action="store_true")
    parser.add_argument("-t", "--try_and_error", help="Try-and-error approach", action="store_true")
    parser.add_argument("-e", "--eq", type=str, help="list of tasks to allocate on the same proc: <task1=task2,task3=task4>")
    args = parser.parse_args()
    main(args.input, args.verbose, args.model, args.show, args.try_and_error, args.beta, args.eq)
