import xml.etree.ElementTree as ET
import argparse

def main(output):
    mytree = ET.parse('ChallengeModel_w_LabelStats_fixedLabelMapping_App4mc_v072.amxmi')
    myroot = mytree.getroot()

    f = open(output, 'w')
    for alloc in myroot.iter('taskAllocation'):
        task = alloc.get('task').split('?')[0]
        core = alloc.get('scheduler').split('?')[0][-1]
        f.write("affinity_" + task + " " + core + "\n")
        
    f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", type=str, help="Output file", default = "./waters_sol.rt")
    args = parser.parse_args()
    main(args.output)
