import argparse
import xml.etree.ElementTree as ET
import math

def time_ceil(value, unit):
    if (unit == 'ns'):
        res = value
    elif (unit == 'us'):
        res = int(math.ceil(value/1000.))
    elif (unit == 'ms'):
        res = int(math.ceil(value/1000000.))
    return res

def time_floor(value, unit):
    if (unit == 'ns'):
        res = value
    elif (unit == 'us'):
        res = int(math.floor(value/1000.))
    elif (unit == 'ms'):
        res = int(math.floor(value/1000000.))
    return res


def main(unit, output, nodata):
    mytree = ET.parse('ChallengeModel_w_LabelStats_fixedLabelMapping_App4mc_v072.amxmi')
    myroot = mytree.getroot()

    # By default values are in ns
    FREQUENCY = float(myroot.find(".//*/frequency").get('value'))
    PLL = (1.0E9 / FREQUENCY)
    task_id = 0

    rt_model = []
    rt_model.append("## Number of processors")
    rt_model.append("processors 4")

    data = {}
    for task in myroot.iter('tasks'):
        tsk_name = task.get('name')
        tsk_priority = task.get('priority')
        tsk_stimuli = task.get('stimuli').split('?')
        if (tsk_stimuli[1] == "type=Periodic"):
            if (tsk_stimuli[0].split('_')[1][-2:] == "us"):
                tsk_period = time_ceil(int(float(tsk_stimuli[0].split('_')[1][:-2]) * 1000), unit)
            elif (tsk_stimuli[0].split('_')[1][-2:] == "ms"):
                tsk_period = time_ceil(int(float(tsk_stimuli[0].split('_')[1][:-2]) * 1000000), unit)
            else:
                print("Wrong unit used for the period")
                quit()
        else:
            if (tsk_stimuli[0].split('_')[1][-2:] == "us"):
                tsk_period = time_ceil(int(float(tsk_stimuli[0].split('_')[1][:-2]) * 1000), unit)
            elif (tsk_stimuli[0].split('_')[1][-2:] == "ms"):
                tsk_period = time_ceil(int(float(tsk_stimuli[0].split('_')[1][:-2]) * 1000000), unit)
            else:
                print("Wrong unit used for the period")
                quit()
        task_id += 1
        task_st = []
        task_st.append("## key for task "+ tsk_name)
        task_st.append("task_" +str(task_id) + " " + tsk_name)
        task_st.append("## key for task priority")
        task_st.append(tsk_name + "_pr " + tsk_priority)
        task_st.append("## key for task period")
        task_st.append(tsk_name + "_per " + str(tsk_period))

        task_st.append("## hyperjobs automaton (sequences of critical sections, not necessarily in the same activity)")
        task_st.append("## sub-automaton 1")

        idx_CS = 0
        run_st = []
        for runnable in task.iter('calls'):
            run_name = runnable.get('runnable').split('?')[0]
            # Really ugly trick to manage successors
            if idx_CS != 0 :
                run_st[-1] = run_st[-1] + run_name
            idx_CS += 1
            
            print(">>>> " + run_name)
            st = ".//*[@name='"+run_name+"']"
            runnable = myroot.findall(st)[0]
            #print(runnable.attrib)
            bcet = float(runnable.findall(".//*/lowerBound")[0].get('value'))
            bcet = time_floor(int(bcet * PLL), unit)
            wcet = float(runnable.findall(".//*/upperBound")[0].get('value'))
            wcet = time_ceil(int(wcet * PLL), unit)
            # print(runnable.iter('lowerBound')[0].attrib)
            run_st.append("## key for critical section "+ run_name)
            run_st.append("CS_" + str(task_id) + "_1_" + str(idx_CS) + " " + run_name)
            run_st.append("## key for bcet/wcet")
            run_st.append(run_name + "_bcet " + str(bcet))
            run_st.append(run_name + "_wcet " + str(wcet))

            if (not nodata):
                run_st.append("## keys for data")
                for e in runnable.findall(".//*/[@data]"):            
                    data_name = e.get('data').split('?')[0]            
                    access = e.get('access')
                    if (access == 'read'):
                        run_st.append(run_name + "_in " + data_name)
                        value = e.findall(".//*/[@value]")[0].get('value')
                        data[data_name] = value
                        # print(data_name, access, value)
                    else:
                        run_st.append(run_name + "_out " + data_name)
                        # print(data_name, access  + data_name)

            run_st.append("## keys for successors")
            num = int(run_name.split('_')[-1])
            run_st.append(run_name + "_succ_1 ")

        run_st[-1] = run_st[-1] + "END_" + tsk_name
        run_st.append("## key for (last pseudo) CS in the sub-automaton")
        run_st.append("CS_" + str(task_id) + "_1_" + str(idx_CS+1) + " END_" + tsk_name)

        task_st += run_st
        task_st.append("\n")
        rt_model += task_st


    run_st.append("## data penalities")
    for key, value in data.items():
        rt_model.append(key + "_penalty " + str(time_ceil(int(float(value) * PLL), unit)))

    #print("\n".join(rt_model))
    f = open(output, 'w')
    f.write("\n".join(rt_model))
    f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--unit", type=str, help="Time unit (ms, us, ns). By default ns is used", default = "ns")
    parser.add_argument("-o", "--output", type=str, help="Output file", default = "./waters.rt")
    parser.add_argument("-nd", "--nodata", help="Do not consider data", action="store_true", default = False)
    args = parser.parse_args()
    main(args.unit, args.output, args.nodata)